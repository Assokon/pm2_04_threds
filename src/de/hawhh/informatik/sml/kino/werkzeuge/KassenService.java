package de.hawhh.informatik.sml.kino.werkzeuge;

import java.util.Set;

import de.hawhh.informatik.sml.kino.fachwerte.platz.Platz;
import de.hawhh.informatik.sml.kino.materialien.Vorstellung;



public class KassenService extends ObservableKassenService
{
    
    public void reservierePlaetze(Vorstellung ausgewaehlteVorstellung, Set<Platz> plaetze)
    {
        ausgewaehlteVorstellung.reservierePlaetze(plaetze);
        informiereUeberAenderung();
    }
    
    public void stornierePlaetze(Vorstellung ausgewaehlteVorstellung, Set<Platz> plaetze)
    {
        ausgewaehlteVorstellung.stornierePlaetze(plaetze);
        informiereUeberAenderung();
    }
    
    public void verkaufePlaetze(Vorstellung ausgewaehlteVorstellung, Set<Platz> plaetze)
    {
        ausgewaehlteVorstellung.verkaufePlaetze(plaetze);
        informiereUeberAenderung();
    }
    
    public void gibPlaetzeFrei(Vorstellung ausgewaehlteVorstellung, Set<Platz> freiZuGebendePlaetze)
    {
        ausgewaehlteVorstellung.gibPlaetzeFrei(freiZuGebendePlaetze);
        informiereUeberAenderung();
    }
    
    public void aktualisiereGesamtPreisanzeige()
    {
        informiereUeberPreisanzeige();
    }

    public void speichereAuswahl()
    {
        speichereAusgewaehltePlaetze();
    }

    public void aktualisereAlle()
    {
        informiereUeberAenderung();
        
    }
}
