package de.hawhh.informatik.sml.kino.werkzeuge.barzahlung;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import de.hawhh.informatik.sml.kino.werkzeuge.VerkaufsListener;

public class BarzahlungsWerkzeug{

    private BarzahlungUI _barzahlungsUI;
    private double _preis;
    private List<VerkaufsListener> _verkaufsListener;
    
    /**
     * Initialisiert das Barzahlungswerkzeug
     * @param owner
     *            das Elternfenster
     *            
     * @rquire owner != null
     *            
     * @ensure _barzahlungsUI != null
     * 
     */
    public BarzahlungsWerkzeug(JFrame owner)
    {
        assert owner != null : "Vorbedingung verletzt: owner != null";
        
        _barzahlungsUI = new BarzahlungUI(owner);
        assert _barzahlungsUI != null : "Vorbedingung verletzt: _barzahlungsUI != null";
        
        _verkaufsListener = new LinkedList<VerkaufsListener>();
        registriereListener();
    }

    /**
     * Registriert die Listener der Textfelder und die UI-Aktionen
     */
    private void registriereListener()
    {
        _barzahlungsUI.getBezahltEingabe().getDocument().addDocumentListener(new DocumentListener()
        {
            
            @Override
            public void removeUpdate(DocumentEvent e)
            {
                berechneRueckeld();
            }
            
            @Override
            public void insertUpdate(DocumentEvent e)
            {
                berechneRueckeld();
                
            }
            
            @Override
            public void changedUpdate(DocumentEvent e)
            {
                berechneRueckeld();
                
            }
        });
        
        _barzahlungsUI.getOkButton().addActionListener(new ActionListener()
        {
            
            @Override
            public void actionPerformed(ActionEvent e)
            {
                informiereUeberVerkauf();
                schliessen();
                
            }
        });
        
        _barzahlungsUI.getAbbrechenButton().addActionListener(new ActionListener()
        {
            
            @Override
            public void actionPerformed(ActionEvent e)
            {
                schliessen();
                
            }
        });
    }
    
    /**
     * Berechnet das Rueckgeld.
     */
    private void berechneRueckeld()
    {
            if(!_barzahlungsUI.getBezahltEingabe().getText().isEmpty())
            {
                double bezahlt = _barzahlungsUI.getBezahltGeldbetrag();
                if(_barzahlungsUI.getBezahltEingabe().getText().matches("\\d*"))
                {
                    if (_barzahlungsUI.getBezahltGeldbetrag()>Integer.MAX_VALUE)
                    {
                        _barzahlungsUI.setRueckgeldEingabe("Der eingegebene Betrag ist zu groß.");
                    }
                    else
                    {
                        _barzahlungsUI.setRueckgeldEingabe(String.format("%.2f",bezahlt - _preis));
                    }
                }
                else
                {
                    _barzahlungsUI.getRueckgeldEingabe().setText("Bitte nur ganze Zahlen eingeben!");
                }
            
                if(bezahlt >= _preis)
                {
                    _barzahlungsUI.getOkButton().setEnabled(true);
                }
                else
                {
                    _barzahlungsUI.getOkButton().setEnabled(false);
                }
            }
            else
            {
                _barzahlungsUI.setRueckgeldEingabe(String.format("-%.2f", _preis));
            }
        }
    
    /**
     * Fügt einen Verkaufslistener hinzu, der bei Verkauf informiert
     * 
     * @param verkaufsListener
     */
    public void registriereVerkaufsServiceObserver(VerkaufsListener verkaufsListener)
    {
        assert verkaufsListener != null : "Vorbedingung verletzt: verkaufsServiceObserver != null";
        _verkaufsListener.add(verkaufsListener);
    }
    
    public void informiereUeberVerkauf()
    {
        for (VerkaufsListener verkaufsListener : _verkaufsListener)
        {
            verkaufsListener.reagiereAufVerkauf();
        }
    }

    /**
     * Gibt das Barzahlungsfenster zurück.
     * @return BarzahlungsUI _barzahlungsUI
     */

    /**
     * Startet die Barzahlung
     * @param preis
     */
    public void starteZahlung(double preis)
    {
        _barzahlungsUI.setPreisBetrag(preis);
        _barzahlungsUI.resetBezahltEingabe(0);
        _preis = preis;
        _barzahlungsUI.setRueckgeldEingabe(String.format("-%.2f", _preis));
        _barzahlungsUI.getOkButton().setEnabled(false);
        JFrame owner = _barzahlungsUI.getOwner();
        _barzahlungsUI.getDialog().setLocationRelativeTo(owner);
        macheSichtbar();
    }

    /**
     * Gibt die GUI des Barzahlungswerkzeugs.
     * 
     * @return _barzahlungsUI
     */
    public JDialog getUI()
    {
        return _barzahlungsUI.getDialog();
    }

    /**
     * Versteckt die GUI des Barzahlungswerkzeugs.
     */
    public void schliessen()
    {
        _barzahlungsUI.getDialog().dispose();
    }

    /**
     * Holt die GUI des Barzahlungswerkzeugs nach vorne.
     */
    public void macheSichtbar()
    {
        _barzahlungsUI.getDialog().setVisible(true);
    }
    
}
