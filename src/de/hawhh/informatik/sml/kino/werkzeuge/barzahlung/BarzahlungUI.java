package de.hawhh.informatik.sml.kino.werkzeuge.barzahlung;

import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BarzahlungUI{

    private JDialog _dialog;
    private JPanel _panel;
    private JButton _ok;
    private JButton _abbrechen;
    private JTextField _preisBetrag;
    private JTextField _bezahltEingabe;
    private JTextField _rueckgeldEingabe;
    private JFrame _owner;
    
    
    /**
     * Initialisiert das Barzahlungsfenster
     */
    public BarzahlungUI(JFrame owner)
    {   
        _owner = owner;
        
        _dialog = new JDialog(_owner);
        _dialog.setTitle("Barzahlung");
        _dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
        _dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        _dialog.setMinimumSize(new Dimension(500,20));
        _dialog.setResizable(false);

        _panel = new JPanel(new GridLayout(4, 2));
        
        _preisBetrag = new JTextField();
        _preisBetrag.setEditable(false);
        
        _bezahltEingabe = new JFormattedTextField(0);
        
        _rueckgeldEingabe = new JFormattedTextField();
        _rueckgeldEingabe.setEditable(false);

        _ok = new JButton("OK");
        _ok.setEnabled(false);
        _abbrechen = new JButton("Abbrechen");
        
        _panel.add(new JLabel("Preisbetrag"));
        _panel.add(_preisBetrag);
        _panel.add(new JLabel("Bezahlt"));
        _panel.add(_bezahltEingabe);
        _panel.add(new JLabel("Rueckgeld"));
        _panel.add(_rueckgeldEingabe);
        _panel.add(_ok);
        _panel.add(_abbrechen);
        
        _dialog.add(_panel);
        _dialog.pack();
    }
    
    
    /**
     * Gibt Ok-Button zurück
     * @return JButton _ok
     */
    public JButton getOkButton() {
        return _ok;
    }

    /**
     * Gibt Abbrechen-Button zurück
     * @return JButton _abbrechen
     */
    public JButton getAbbrechenButton() {
        return _abbrechen;
    }

    /**
     * Gibt das Eingabetextfeld zurück
     * @return JTextField _bezahltEingabe
     */
    public JTextField getBezahltEingabe() {
        return _bezahltEingabe;
    }

    /**
     * Setzt das Eingabefeld auf einen gegeben Wert zurück.
     * @param bezahltEingabe
     */
    public void resetBezahltEingabe(int bezahltEingabe) {
        _bezahltEingabe.setText(String.valueOf(bezahltEingabe));
    }
    
    /**
     * Wandelt den String im Eingabetextfeld in einen Double um und
     * gibt ihn zurück.
     * @return Doublewert von Eingabetextfeld
     */
    public double getBezahltGeldbetrag()
    {
        try
        {
            return Double.valueOf(_bezahltEingabe.getText());
        } 
        catch (Exception e)
        {
            return 0.0;
        }
    }

    /**
     * Gibt das Rueckgeldtextfeld zurück
     * @return JTextField _rueckgeldEingabe
     */
    public JTextField getRueckgeldEingabe() {
        return _rueckgeldEingabe;
    }
    
    /**
     * Wandelt den String im Rueckgeldtextfeld in einen Double um und
     * gibt ihn zurück.
     * @return Doublewert von Rueckgeldtextfeld
     */
    public double getRueckgeldGeldbetrag()
    {   
        try
        {
            return Double.valueOf(_rueckgeldEingabe.getText());
        } 
        catch (Exception e)
        {
            return 0.0;
        }
        
    }

    /**
     * Setzt das Rueckgeld.
     * @param String rueckgeldEingabe
     */
    public void setRueckgeldEingabe(String rueckgeldEingabe) {
        assert rueckgeldEingabe != null : "Vorbedingung verletzt: rueckgeldEingabe != null";
        _rueckgeldEingabe.setText(rueckgeldEingabe);
    }
    
    
    /**
     * Setzt den Preisbetrag 
     * @param preisBetrag
     */
    public void setPreisBetrag(double preisBetrag)
    {
        _preisBetrag.setText(""+preisBetrag);
    }
    
    public JDialog getDialog()
    {
        return _dialog;
    }
    
    public JFrame getOwner()
    {
        return _owner;
    }
}
