package de.hawhh.informatik.sml.kino.werkzeuge;

public interface KassenServiceObserver
{

    /**
     * Reagiert auf eine Änderung im beobachteten KassenService
     */
    void reagiereAufAenderung();
    
    /**
     * Aktualisiert die Preisanzeige
     */
    void aktualisierePreisanzeige();
    
    /**
     * Speichert die Auswahl
     */
    void speichereAuswahl();
    
}
