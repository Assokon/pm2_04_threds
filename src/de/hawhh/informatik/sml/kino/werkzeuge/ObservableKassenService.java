package de.hawhh.informatik.sml.kino.werkzeuge;

import java.util.HashSet;
import java.util.Set;

public abstract class ObservableKassenService
{

    private Set<KassenServiceObserver> _alleBeobachter;

    /**
     * Initialisiert ein beobachtbaren KassenService.
     */
    public ObservableKassenService()
    {
        _alleBeobachter = new HashSet<KassenServiceObserver>();
    }

    /**
     * Registriert einen Beobachter. Der Beobachter wird
     * informiert, wenn sich etwas ändert.
     * 
     * @require beobachter != null
     */
    public void registriereBeobachter(KassenServiceObserver beobachter)
    {
        assert beobachter != null : "Vorbedingung verletzt: beobachter != null";
        _alleBeobachter.add(beobachter);
    }

    /**
     * Entfernt einen Beobachter. Wenn der Beobachter gar
     * nicht registriert war, passiert nichts.
     */
    public void entferneBeobachter(KassenServiceObserver beobachter)
    {
        _alleBeobachter.remove(beobachter);
    }

    /**
     * Informiert alle registrierten Beobachter über eine
     * Änderung.
     */
    protected void informiereUeberAenderung()
    {
        for (KassenServiceObserver beobachter : _alleBeobachter)
        {
            beobachter.reagiereAufAenderung();
        }
    }
    
    /**
     * Informiert alle registrierten Beobachter ihre Preisanzeige zu aktualisieren.
     */
    protected void informiereUeberPreisanzeige()
    {
        for (KassenServiceObserver beobachter : _alleBeobachter)
        {
            beobachter.aktualisierePreisanzeige();
        }
    }
    
    /**
     * Informiert alle registrierten Beobachter, ihre ausgewaehlten Plaetze zu speichern.
     */
    protected void speichereAusgewaehltePlaetze()
    {
        for (KassenServiceObserver beobachter : _alleBeobachter)
        {
            beobachter.speichereAuswahl();
        }
    }
    
}
