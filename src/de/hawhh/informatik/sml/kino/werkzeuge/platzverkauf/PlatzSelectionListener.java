package de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf;

import java.util.EventListener;

/**
 * Interface eines Listeners, der bei Änderungen der Platzauswahl benachrichtigt
 * wird.
 * 
 * @author SE2-Team, PR2-Team
 * @version WiSe 2014
 */
interface PlatzSelectionListener extends EventListener
{
    /**
     * Wird aufgerufen, wenn sich die Auswahl geändert hat.
     */
    void auswahlGeaendert();
    
    void auswahlKassenService(PlatzSelectionEvent event);
}
