package de.hawhh.informatik.sml.kino.werkzeuge.platzverkauf;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;

import de.hawhh.informatik.sml.kino.fachwerte.platz.Platz;
import de.hawhh.informatik.sml.kino.materialien.Kinosaal;
import de.hawhh.informatik.sml.kino.materialien.Vorstellung;
import de.hawhh.informatik.sml.kino.werkzeuge.KassenService;
import de.hawhh.informatik.sml.kino.werkzeuge.KassenServiceObserver;
import de.hawhh.informatik.sml.kino.werkzeuge.VerkaufsListener;
import de.hawhh.informatik.sml.kino.werkzeuge.barzahlung.BarzahlungsWerkzeug;

/**
 * Mit diesem Werkzeug können Plätze verkauft und storniert werden. Es arbeitet
 * auf einer Vorstellung als Material. Mit ihm kann angezeigt werden, welche
 * Plätze schon verkauft und welche noch frei sind.
 * 
 * Dieses Werkzeug ist ein eingebettetes Subwerkzeug. Es kann nicht beobachtet
 * werden.
 * 
 * @author SE2-Team, PR2-Team
 * @version WiSe 2014
 */
public class PlatzVerkaufsWerkzeug
{
    // Die aktuelle Vorstellung, deren Plätze angezeigt werden. Kann null sein.
    private Vorstellung _vorstellung;

    private PlatzVerkaufsWerkzeugUI _ui;

    private BarzahlungsWerkzeug _barzahlungsWerkzeug;

    private KassenService _kassenService;

    private double _preis;
    
    private Set<Platz> _ausgeWaehltePlaetze;
    
    
    /**
     * Beschreibung:
     * _ausgewaehltePlaetzeAlterStand
     * Enthaelt immer die vorherige Auswahl und wird bei jeder
     * neuen Auswahl entsprechend befüllt.
     * Wird aber ein Button beispielsweise beim Stornieren manipuliert,
     * muss dieser ebenfalls aus dem alten Stand raus.
     * 
     * _alterStandLeer
     * Bei Aktionen, wo keine Auswahl mehr besteht,
     * muss auch der alte Stand gelöscht werden.
     */
    private Set<Platz> _ausgewaehltePlaetzeAlterStand;

    /**
     * Initialisiert das PlatzVerkaufsWerkzeug.
     * 
     * @param kassenService
     */
    public PlatzVerkaufsWerkzeug(KassenService kassenService)
    {
        _ui = new PlatzVerkaufsWerkzeugUI();
        _kassenService = kassenService;
        _ausgewaehltePlaetzeAlterStand = new HashSet<Platz>();
        _ausgeWaehltePlaetze = new HashSet<Platz>();
        _barzahlungsWerkzeug = null;
        registriereUIAktionen();
        // Am Anfang wird keine Vorstellung angezeigt:
        setVorstellung(null);
        registriereKassenServiceListener();
    }
    
    
    /**
     * Registrieren der KassenServiceListener
     */
    private void registriereKassenServiceListener()
    {
        _kassenService.registriereBeobachter(new KassenServiceObserver()
        {
            @Override
            public void reagiereAufAenderung()
            {
                aktualisierePlatzplan();
            }

            @Override
            public void aktualisierePreisanzeige()
            {
                reagiereAufNeuePlatzAuswahl();
            }

            @Override
            public void speichereAuswahl()
            {
                _ausgeWaehltePlaetze = ausgewaehltePlaetze();
            }
        });

    }
    
    /**
     * Registrieren der Verkaufslistener
     */
    private void registriereVerkaufsListener()
    {
        _barzahlungsWerkzeug.registriereVerkaufsServiceObserver(new VerkaufsListener()
        {
            
            @Override
            public void reagiereAufVerkauf()
            {
                _ausgeWaehltePlaetze.clear();
                _ausgewaehltePlaetzeAlterStand.clear();
                Set<Platz> plaetze = ausgewaehltePlaetze();
                _kassenService.verkaufePlaetze(_vorstellung, plaetze);
                reagiereAufNeuePlatzAuswahl();
            }
        });
    }

    /**
     * Gibt das Panel dieses Subwerkzeugs zurück. Das Panel sollte von einem
     * Kontextwerkzeug eingebettet werden.
     * 
     * @ensure result != null
     */
    public JPanel getUIPanel()
    {
        return _ui.getUIPanel();
    }

    /**
     * Fügt der UI die Funktionalität hinzu mit entsprechenden Listenern.
     */
    private void registriereUIAktionen()
    {
        _ui.getVerkaufenButton().addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                _barzahlungsWerkzeug.starteZahlung(_preis / 100);
            }
        });

        _ui.getStornierenButton().addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Set<Platz> plaetze = ausgewaehltePlaetze();
                _kassenService.stornierePlaetze(_vorstellung, plaetze);
                _kassenService.gibPlaetzeFrei(_vorstellung, plaetze);
                
                for (Platz platz : plaetze)
                {
                    _ui.getPlatzplan().markierePlatzAlsFrei(platz);
                }
                _ausgeWaehltePlaetze.clear();
                _ausgewaehltePlaetzeAlterStand.clear();
                
                reagiereAufNeuePlatzAuswahl();
                _kassenService.aktualisereAlle();
                _kassenService.aktualisiereGesamtPreisanzeige();
            }
        });

        _ui.getPlatzplan().addPlatzSelectionListener(new PlatzSelectionListener()
        {
            @Override
            public void auswahlGeaendert()
            {   
                reagiereAufNeuePlatzAuswahl();
            }

            @Override
            public void auswahlKassenService(PlatzSelectionEvent event)
            {
                if(_ausgewaehltePlaetzeAlterStand.isEmpty())
                {
                    _ausgewaehltePlaetzeAlterStand.addAll(event.getAusgewaehltePlaetze());
                }
                
                //Alle Werkzeuge speichern ihre Ausgewaehlten Plaetze
                _kassenService.speichereAuswahl();
                
                if (_vorstellung != null)
                {
                    if (_vorstellung.sindVerkaufbar(event.getAusgewaehltePlaetze()))
                    {
                        //Der lässt über all reservieren
                        _kassenService.reservierePlaetze(_vorstellung, event.getAusgewaehltePlaetze());
                    }

                    // Alle nicht (mehr) ausgewaehlten freigeben
                    Set<Platz> freizuGebendePlaetze = freiZuGebendePlaetzeNachDeselektion(event.getAusgewaehltePlaetze());
                    _kassenService.gibPlaetzeFrei(_vorstellung, freizuGebendePlaetze);
                    
                    //Eigene Plaetze als Ausgewaehlt markieren
                    Kinosaal saal = _vorstellung.getKinosaal();
                    for (Platz platz : saal.getPlaetze())
                    {
                        if (event.getAusgewaehltePlaetze().contains(platz))
                        {
                            _ui.getPlatzplan().markierePlatzAlsAusgewaehlt(platz);
                        }
                    }
                    
                }
            }
        });
    }

    /**
     * Reagiert darauf, dass sich die Menge der ausgewählten Plätze geändert
     * hat.
     * 
     * @param plaetze
     *            die jetzt ausgewählten Plätze.
     */
    private void reagiereAufNeuePlatzAuswahl()
    {   
        _ui.getVerkaufenButton().setEnabled(istVerkaufenMoeglich(ausgewaehltePlaetze()));
        _ui.getStornierenButton().setEnabled(istStornierenMoeglich(ausgewaehltePlaetze()));
        aktualisierePreisanzeige(ausgewaehltePlaetze());
    }

    /**
     * Aktualisiert den anzuzeigenden Gesamtpreis
     */
    private void aktualisierePreisanzeige(Set<Platz> plaetze)
    {

        if (istVerkaufenMoeglich(plaetze))
        {
            _preis = _vorstellung.getPreisFuerPlaetze(plaetze);
            _ui.getPreisLabel().setText("Gesamtpreis: " + String.format("%.2f", _preis / 100.0) + " Euro");
        } 
        else
        {
            _ui.getPreisLabel().setText("Gesamtpreis:");
        }
    }

    /**
     * Prüft, ob die angegebenen Plätze alle storniert werden können.
     */
    private boolean istStornierenMoeglich(Set<Platz> plaetze)
    {
        return !plaetze.isEmpty() && _vorstellung.sindStornierbar(plaetze);
    }

    /**
     * Prüft, ob die angegebenen Plätze alle verkauft werden können.
     */
    private boolean istVerkaufenMoeglich(Set<Platz> plaetze)
    {
        return !plaetze.isEmpty() && _vorstellung.sindVerkaufbar(plaetze);
    }

    

    /**
     * Aktualisiert den Platzplan basierend auf der ausgwählten Vorstellung.
     */
    private void aktualisierePlatzplan()
    {
        if (_vorstellung != null)
        {
            Kinosaal saal = _vorstellung.getKinosaal();

            for (Platz platz : saal.getPlaetze())
            {
                if (_vorstellung.istPlatzVerkauft(platz))
                {
                    _ui.getPlatzplan().markierePlatzAlsVerkauft(platz);
                }
                
                if(_vorstellung.istPlatzReserviert(platz) && !_vorstellung.istPlatzVerkauft(platz))
                {
                    _ui.getPlatzplan().markierePlatzAlsReserviert(platz);
                }
                
                if(!_vorstellung.istPlatzReserviert(platz) && !_vorstellung.istPlatzVerkauft(platz))
                {
                    _ui.getPlatzplan().markierePlatzAlsFrei(platz);
                    _ausgeWaehltePlaetze.remove(platz);
                    _ausgewaehltePlaetzeAlterStand.remove(platz);
                }
                
                if(_ausgeWaehltePlaetze != null)
                {
                    if(_ausgeWaehltePlaetze.contains(platz))
                    {
                        _ui.getPlatzplan().markierePlatzAlsAusgewaehlt(platz);
                    }
                }
            }
        } 
        else
        {
            _ui.getPlatzplan().setAnzahlPlaetze(0, 0);
        }
    }
    
    
    /**
     * Aktualisiert den Platzplan nach einem WECHSEL basierend auf der ausgwählten Vorstellung.
     */
    private void aktualisierePlatzplanNachWechsel()
    {
        if (_vorstellung != null)
        {
            Kinosaal saal = _vorstellung.getKinosaal();
            _ui.getPlatzplan().setAnzahlPlaetze(saal.getAnzahlReihen(), saal.getAnzahlSitzeProReihe());

            for (Platz platz : saal.getPlaetze())
            {
                if(_vorstellung.istPlatzVerkauft(platz))
                {
                    _ui.getPlatzplan().markierePlatzAlsVerkauft(platz);
                }
                
                if(_vorstellung.istPlatzReserviert(platz))
                {
                    _ui.getPlatzplan().markierePlatzAlsReserviert(platz);
                }
            }
        } 
        else
        {
            _ui.getPlatzplan().setAnzahlPlaetze(0, 0);
        }
    }

    
    /**
     * Gibt die noch ausgewaehlten Plaetze (aber nicht verkauften!) 
     * bei einem Vorstellungswechsel wieder frei.
     * 
     * @param ausgewaehltePlaetze
     * @return freizuGebendePlaetze
     */
    private Set<Platz> freiZuGebendePlaetzeNachWechsel(Set<Platz> ausgewaehltePlaetze)
    {
        Kinosaal saal = _vorstellung.getKinosaal();
        Set<Platz> freizuGebendePlaetze = new HashSet<Platz>();

        for (Platz platz : saal.getPlaetze())
        {
            if (ausgewaehltePlaetze.contains(platz))
            {
                freizuGebendePlaetze.add(platz);
            }
        }
        return freizuGebendePlaetze;
    }
    
    
    /**
     * Gleicht anhand des alten Stands mit den derzeit ausgewaehlten Plätzen ab,
     * welche wieder freigegegben werden muessen.
     * 
     * @param ausgewaehltePlaetze
     * @return freizuGebendePlaetze
     */
    private Set<Platz> freiZuGebendePlaetzeNachDeselektion(Set<Platz> ausgewaehltePlaetze)
    {
        Kinosaal saal = _vorstellung.getKinosaal();
        Set<Platz> freizuGebendePlaetze = new HashSet<Platz>();
        for (Platz platz : saal.getPlaetze())
        {
            if (!ausgewaehltePlaetze.contains(platz) && _ausgewaehltePlaetzeAlterStand.contains(platz))
            {
                freizuGebendePlaetze.add(platz);
            }
        }
        _ausgewaehltePlaetzeAlterStand.clear();
        _ausgewaehltePlaetzeAlterStand.addAll(ausgewaehltePlaetze);
        return freizuGebendePlaetze;
    }

    /**
     * Setzt die Vorstellung. Sie ist das Material dieses Werkzeugs. Wenn die
     * Vorstellung gesetzt wird, muss die Anzeige aktualisiert werden. Die
     * Vorstellung darf auch null sein.
     */
    public void setVorstellung(Vorstellung vorstellung)
    {
        if (_vorstellung != null)
        {
            //Wenn ausgewaehlte Plaetze noch nicht gekauft wurden, muessen diese wieder freigegeben werden
            Set<Platz> ausgewaehltePlaetze = ausgewaehltePlaetze();
            Set<Platz> freizuGebendePlaetze = freiZuGebendePlaetzeNachWechsel(ausgewaehltePlaetze);
            _kassenService.gibPlaetzeFrei(_vorstellung, freizuGebendePlaetze);
        }
        _vorstellung = vorstellung;
        
        //eigene Auswahl wieder löschen, da speichere Auswahl sie noch hat.
        if(_ausgeWaehltePlaetze != null)
        {
            _ausgeWaehltePlaetze.clear();
        }
        aktualisierePlatzplanNachWechsel();
    }
    
    /**
     * Setzt für _owner ein JFrame, welches dazu gedacht ist als Elternfenster
     * für ein anderes Fenster benutzt zu werden.
     * 
     * @param owner
     *            das Elternfenster
     */
    public void setOwner(JFrame owner)
    {
        _barzahlungsWerkzeug = new BarzahlungsWerkzeug(owner);
        registriereVerkaufsListener();
    }
    
    public Set<Platz> ausgewaehltePlaetze()
    {
        return _ui.getPlatzplan().getAusgewaehltePlaetze();
    }
    
    
    /**
     * Methode für das KassenWerkzeug, um vor dem Schliessen
     * ausgewaehlte Plaetze wieder freizugeben.
     */
    public void gibReservierteNachSchließenFrei()
    {
        _kassenService.gibPlaetzeFrei(_vorstellung, ausgewaehltePlaetze());
    }
}
