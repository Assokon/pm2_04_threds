package de.hawhh.informatik.sml.kino.werkzeuge;

public interface VerkaufsListener
{
    /**
     * Reagiert auf einen Verkauf
     */
    void reagiereAufVerkauf();
}
